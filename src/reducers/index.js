import {combineReducers} from 'redux';

const songsReducer = () => {
    return [
        {
            title: 'Rebirth',
            duration: '3:37'
        },
        {
            title: 'UV',
            duration: '5:14'
        },
        {
            title: 'Breathe Us To Life',
            duration: '3:54'
        },
        {
            title: 'Sunrise At Cala Bassa',
            duration: '5:43'
        }
    ];
}

const selectedSongReducer = (selectedSong = null, action) => {
    if(action.type === 'SONG_SELECTED'){
        return action.payload;
    }

    return selectedSong;
}

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});